﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeightedChanceConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            const int iterations = 1000;
            List<Item> Items = new List<Item>();
            Items.Add(new Item("NOTHING", 50.0f));
            Items.Add(new Item("BOOGEYMAN", 12.5f));
            Items.Add(new Item("KEY", 10.0f));
            Items.Add(new Item("WATER GUN", 7.25f));
            Items.Add(new Item("WEED WHACKER", 2.00f));
            Items.Add(new Item("DOLLAR", 2.00f));
            Items.Add(new Item("SODA CANS", 2.00f));
            Items.Add(new Item("MEDICAL KIT", 2.00f));
            Items.Add(new Item("GOLD COINS", 1.50f));
            Items.Add(new Item("ANCIENT ARTIFACT", 1.50f));
            Items.Add(new Item("BAZOOKA", 1.00f));
            Items.Add(new Item("MYSTERY POTION", 1.00f));
            Items.Add(new Item("PANDORA'S BOX", 1.00f));
            Items.Add(new Item("CLOWN KIT", 1.00f));
            Items.Add(new Item("SPEED SHOES", 1.00f));
            Items.Add(new Item("MONSTER POTION", 1.00f));
            Items.Add(new Item("TOMATO", 0.50f));
            Items.Add(new Item("POPSICLE", 0.50f));
            Items.Add(new Item("FIRE EXTINGUISHER", 0.50f));
            Items.Add(new Item("DISHES", 0.50f));
            Items.Add(new Item("SILVERWARE", 0.50f));
            Items.Add(new Item("GHOST POTION", 0.50f));
            Items.Add(new Item("SKULL KEY", 0.25f));

            // total the weigth
            int totalWeight = 0;
            foreach (var item in Items)
            {
                totalWeight += item.Weight;
            }

            while (true)
            {
                //var result = new Dictionary<string, int>();

                // Trial #1: Pick single item

                var selectedItem = WeightChanceWorkflow.GetItem(Items, totalWeight);
                Console.WriteLine($"Item: {selectedItem.Name}");

                // Trial 2: Try # of iterations, and show my stats)
                //for (var i = 0; i < iterations; i++)
                //{
                //    var selectedItem = WeightChanceWorkflow.GetItem(Items, totalWeight);
                //    if (selectedItem != null)
                //    {
                //        if (result.ContainsKey(selectedItem.Name))
                //        {
                //            result[selectedItem.Name] = result[selectedItem.Name] + 1;
                //        }
                //        else
                //        {
                //            result.Add(selectedItem.Name, 1);
                //        }
                //    }
                //}

                //foreach (var item in result)
                //{
                //    Console.WriteLine($"{item.Key}\t\t {((float)item.Value/iterations):P}");
                //}

               // result.Clear();
                Console.WriteLine();
                Console.ReadLine();
            }
        }
    }
}
