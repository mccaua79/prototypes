﻿namespace WeightedChanceConsole
{
    public class Item
    {
        public string Name { get; set; }
        public int Weight { get; set; }

        public Item(string name, int weight)
        {
            Name = name;
            Weight = weight;
        }

        public Item(string name, float weight)
        {
            Name = name;
            Weight = (int)(weight * 1000);
        }
    }
}
