﻿using System;
using System.Collections.Generic;

namespace WeightedChanceConsole
{
    internal class WeightChanceWorkflow
    {
        private static readonly Random Rnd = new Random();

        public static Item GetItem(List<Item> items, int totalWeight)
        {
            // totalWeight is the sum of all Items' weight

            var randomNumber = Rnd.Next(0, totalWeight);

            Item selectedItem = null;
            foreach (var item in items)
            {
                if (randomNumber < item.Weight)
                {
                    selectedItem = item;
                    break;
                }

                randomNumber = randomNumber - item.Weight;
            }

            return selectedItem;
        }
    }
}
