﻿using System;
using System.Collections.Generic;

namespace ItemRandomizer
{
	class ItemManager
	{
		public List<string> GetItems()
		{
			return new List<string> {
				"1st Sword",
				"2nd Sword",
				"3rd Sword",
				"4th Sword",
				"Lamp",
				"Bow",
				"Silver Arrows",
				"Power Glove",
				"Titan's Mitt",
				"Moon Pearl",
				"Hammer",
				"Hookshot",
				"Blue Mail",
				"Red Mail",
				"Cane of Somaria",
				"Cane of Byrna",
				"Fire Rod",
				"Ice Rod",
				"Blue Boomerang",
				"Red Boomerang",
				"Mushroom",
				"Magic Powder",
				"Bombos Medallion",
				"Ether Medallion",
				"Quake Medallion",
				"Shovel",
				"Flute",
				"Bug Net",
				"Bottle 1",
				"Bottle 2",
				"Bottle 3",
				"Bottle 4",
				"Sheild",
				"Red Shield",
				"Mirror Shield",
				"Magic Cape",
				"Mirror",
				"Book of Mudora"
			};
		}

		public List<string> RandomizeItems(List<string> baseItems)
		{
			Random rng = new Random();
			return RandomizeItems( baseItems, rng );
		}

		public List<string> RandomizeItems( List<string> baseItems, int seed )
		{

			Random rng = new Random( seed );
			return RandomizeItems( baseItems, rng );
		}

		private List<string> RandomizeItems( List<string> baseItems, Random rng )
		{
			var max = baseItems.Count;
			List<int> randomizedIndices = new List<int>( max );
			List<string> randomizedItemsList = new List<string>( max );
			var i = 0;
			while ( i < max - 1 )
			{
				var newIndex = rng.Next( 0, max - 1 );
				var found = randomizedIndices.Contains( newIndex );
				if (found)
				{
					continue;
				}

				var randomItem = baseItems[newIndex];
				randomizedIndices.Add( newIndex );
				randomizedItemsList.Add( randomItem );
				i++;
			}

			return randomizedItemsList;
		}
	}
}
