﻿using System;

namespace ItemRandomizer
{
	class Program
	{
		static void Main( string[] args )
		{
			Console.WriteLine( "Enter a seed value:" );
			var value = Console.ReadLine();
			var seed = Convert.ToInt32(value);
			Console.WriteLine( $"ALTTP Random Items, seed # {seed}" );
			var itemManager = new ItemManager();
			var items = itemManager.GetItems();
			var randomizedItems = itemManager.RandomizeItems( items, seed );
			foreach (var item in randomizedItems)
			{
				Console.WriteLine( item );
			}
		}
	}
}
