﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AESKeyGen
{
	class Program
	{
		private static readonly int _blockSize = 128;
		private static readonly int _keySize = 256;

		static void Main( string[] args )
		{
			var keys = GenerateKeys();
			Console.WriteLine( "Keys Generated" );
			Console.WriteLine( $"Key: {keys.Key}" );
			Console.WriteLine( $"IV: {keys.Value}" );
		}


		public static KeyValuePair<string, string> GenerateKeys()
		{
			const string chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
			var random = new Random();
			var iv = new string(
				Enumerable.Repeat( chars, _blockSize / 8 )
					.Select( s => s[random.Next( s.Length )] )
					.ToArray() );

			using ( var aes = new AesCryptoServiceProvider() )
			{
				aes.GenerateKey();
				var key = Convert.ToBase64String( aes.Key );
				AesSetup( key, iv, aes );
				key = Convert.ToBase64String( aes.Key );
				return new KeyValuePair<string, string>( key, iv );
			}
		}
		private static void AesSetup( string key, string iv, SymmetricAlgorithm aes )
		{
			aes.BlockSize = _blockSize;
			aes.KeySize = _keySize;
			aes.IV = Encoding.UTF8.GetBytes( iv );
			aes.Key = Convert.FromBase64String( key );
			aes.Mode = CipherMode.CBC;
			aes.Padding = PaddingMode.PKCS7;
		}
	}
}
